project(tests)

# Create a config file so we can specify the GSSAPI mechanism module
# configuration. This will overwrite reading the configs from /etc/gss/mech.d
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/gss_mech.conf.cmake
               ${CMAKE_CURRENT_BINARY_DIR}/gss_mech.conf
               @ONLY)

add_library(torture STATIC torture-cmocka.c torture-gss.c)
target_include_directories(torture
                           PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
                           PRIVATE ${gss-pake-ssp_SOURCE_DIR})
target_link_libraries(torture PRIVATE defaults ${GSSAPI_LIBRARY})

add_subdirectory(unittests)
