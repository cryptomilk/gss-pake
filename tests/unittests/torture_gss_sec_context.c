/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <gssapi/gssapi.h>
#include <stdlib.h>

#include "torture-gss.h"
#include "torture-cmocka.h"

static int setup(void **state)
{
    OM_uint32 major, minor = 0;
    gss_cred_id_t cred;
    gss_buffer_desc psk_buf = {
        .length = 64,
        .value = discard_const(
            "31b186ad36e251b72f9fad457b892dbfff46dbdb92a3ca664e2c768f05d5df41"),
    };
    gss_OID_set_desc mech_set = {
        .count = 1,
        .elements = MECH_SPNEGO,
    };
    gss_buffer_desc name_buf = {
        .length = 4,
        .value = discard_const("host"),
    };
    gss_name_t name;

    major =
        gss_import_name(&minor, &name_buf, GSS_C_NT_HOSTBASED_SERVICE, &name);
    assert_gss_status(major, minor);

    major = gss_acquire_cred_with_password(&minor,
                                           name,
                                           &psk_buf,
                                           0,
                                           &mech_set,
                                           GSS_C_BOTH,
                                           &cred,
                                           NULL,
                                           NULL);
    assert_gss_status(major, minor);

    *state = cred;

    return 0;
}

static int teardown(void **state)
{
    OM_uint32 major, minor = 0;
    gss_cred_id_t cred = *state;

    major = gss_release_cred(&minor, &cred);
    assert_gss_status(major, minor);

    return 0;
}

static void test_gss_sec_context_init(void **state)
{
    OM_uint32 major, minor = 0;
    OM_uint32 flags = GSS_C_REPLAY_FLAG | GSS_C_SEQUENCE_FLAG;
    gss_ctx_id_t ictx = GSS_C_NO_CONTEXT;
    gss_buffer_desc itok = {
        .length = 0,
    };
    gss_buffer_desc buf = {
        .length = 4,
        .value = discard_const("host"),
    };
    gss_name_t name;
    gss_cred_id_t cred = *state;

    major = gss_import_name(&minor, &buf, GSS_C_NT_HOSTBASED_SERVICE, &name);
    assert_gss_status(major, minor);

    major = gss_init_sec_context(&minor,
                                 cred,
                                 &ictx,
                                 name,
                                 MECH_SPNEGO,
                                 flags,
                                 GSS_C_INDEFINITE,
                                 NULL,
                                 NULL,
                                 NULL,
                                 &itok,
                                 NULL,
                                 NULL);
    assert_gss_status(major, minor);
    assert_int_equal(major, GSS_S_CONTINUE_NEEDED);
    assert_non_null(ictx);
    assert_int_not_equal(itok.length, 0);
    assert_non_null(itok.value);

    major = gss_delete_sec_context(&minor, &ictx, GSS_C_NO_BUFFER);
    assert_gss_status(major, minor);
}

static void test_gss_sec_context_step1(void **state)
{
    OM_uint32 major, minor = 0;
    OM_uint32 flags = GSS_C_REPLAY_FLAG | GSS_C_SEQUENCE_FLAG;
    gss_ctx_id_t ictx = GSS_C_NO_CONTEXT;
    gss_ctx_id_t actx = GSS_C_NO_CONTEXT;
    gss_buffer_desc itok = {
        .length = 0,
    };
    gss_buffer_desc atok = {
        .length = 0,
    };
    gss_buffer_desc buf = {
        .length = 4,
        .value = discard_const("host"),
    };
    gss_name_t name;
    gss_cred_id_t cred = *state;

    major = gss_import_name(&minor, &buf, GSS_C_NT_HOSTBASED_SERVICE, &name);
    assert_gss_status(major, minor);

    major = gss_init_sec_context(&minor,
                                 cred,
                                 &ictx,
                                 name,
                                 MECH_SPNEGO,
                                 flags,
                                 GSS_C_INDEFINITE,
                                 NULL,
                                 &atok,
                                 NULL,
                                 &itok,
                                 NULL,
                                 NULL);
    assert_gss_status(major, minor);
    assert_int_equal(major, GSS_S_CONTINUE_NEEDED);
    assert_non_null(ictx);
    assert_int_not_equal(itok.length, 0);
    assert_non_null(itok.value);

    major = gss_accept_sec_context(&minor,
                                   &actx,
                                   cred,
                                   &itok,
                                   GSS_C_NO_CHANNEL_BINDINGS,
                                   NULL,
                                   NULL,
                                   &atok,
                                   NULL,
                                   NULL,
                                   NULL);
    assert_gss_status(major, minor);
    assert_int_equal(major, GSS_S_COMPLETE);
    assert_non_null(actx);
    assert_int_not_equal(atok.length, 0);
    assert_non_null(atok.value);

    major = gss_delete_sec_context(&minor, &ictx, GSS_C_NO_BUFFER);
    assert_gss_status(major, minor);
}

int main(int argc, char *argv[])
{
    int rc;

    const struct CMUnitTest display_status_tests[] = {
        cmocka_unit_test_setup_teardown(test_gss_sec_context_init, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_gss_sec_context_step1, NULL, NULL),
    };

    rc = cmocka_run_group_tests(display_status_tests, setup, teardown);

    return rc;
}
