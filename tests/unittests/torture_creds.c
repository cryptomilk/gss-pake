/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>

#include "torture-cmocka.h"
#include "torture-gss.h"

static void test_gss_pake_acquire_cred(void **state)
{
    OM_uint32 major, minor = 0;
    gss_buffer_desc psk_buf = {
        .length = 64,
        .value = discard_const(
            "31b186ad36e251b72f9fad457b892dbfff46dbdb92a3ca664e2c768f05d5df41"),
    };
    gss_OID_set_desc mech_set = {
        .count = 1,
        .elements = MECH_SPNEGO,
    };
    gss_buffer_desc name_buf = {
        .length = 4,
        .value = discard_const("host"),
    };
    gss_name_t name = NULL;
    struct gss_pake_cred *cred = NULL;

    major =
        gss_import_name(&minor, &name_buf, GSS_C_NT_HOSTBASED_SERVICE, &name);
    assert_gss_status(major, minor);

    major = gss_pake_acquire_cred_with_password(&minor,
                                                name,
                                                &psk_buf,
                                                0,
                                                &mech_set,
                                                GSS_C_BOTH,
                                                (gss_cred_id_t *)&cred,
                                                NULL,
                                                NULL);
    assert_gss_status(major, minor);
    assert_non_null(cred);
    assert_int_equal(cred->psk.length, 64);
    assert_non_null(cred->psk.data);

    major = gss_pake_release_cred(&minor, (gss_cred_id_t *)&cred);
    assert_gss_status(major, minor);
}

int main(int argc, char *argv[])
{
    int rc;

    const struct CMUnitTest pake_acquire_cred_tests[] = {
        cmocka_unit_test_setup_teardown(test_gss_pake_acquire_cred, NULL, NULL),
    };

    rc = cmocka_run_group_tests(pake_acquire_cred_tests, NULL, NULL);

    return rc;
}
