function(ADD_CMOCKA_TEST_ENVIRONMENT _TEST_NAME)
    set_property(TEST ${_TEST_NAME}
                 PROPERTY
                    ENVIRONMENT
                    "GSS_MECH_CONFIG=${tests_BINARY_DIR}/gss_mech.conf;LSAN_OPTIONS=suppressions=${tests_SOURCE_DIR}/suppressions/lsan.supp")
endfunction()

include_directories(${tests_SOURCE_DIR}
                    ${gss-pake-ssp_SOURCE_DIR})

# Tests using the gss_pake API directly
add_cmocka_test(torture_creds
                SOURCES torture_creds.c
                LINK_LIBRARIES defaults torture cmocka pake)

# Tests using the GSSAPI going through SPNEGO/NegoEx
add_cmocka_test(torture_gss_creds
                SOURCES torture_gss_creds.c
                LINK_LIBRARIES defaults torture cmocka)
add_cmocka_test_environment(torture_gss_creds)


add_cmocka_test(torture_gss_sec_context
                SOURCES torture_gss_sec_context.c
                LINK_LIBRARIES defaults torture cmocka)
add_cmocka_test_environment(torture_gss_sec_context)
