/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <gssapi/gssapi_ext.h>
#include <stdlib.h>

#include "gss-pake.h"

/*
 * A mech can make itself negotiable via NegoEx (draft-zhu-negoex) by
 * implementing the following three SPIs, and also implementing
 * gss_inquire_sec_context_by_oid() and answering the GSS_C_INQ_NEGOEX_KEY and
 * GSS_C_INQ_NEGOEX_VERIFY_KEY OIDs.  The answer must be in two buffers: the
 * first contains the key contents, and the second contains the key enctype as
 * a four-byte little-endian integer.
 *
 * The negoex_init() function called by the initiator calls the gss functions
 * in the following order:
 *
 *     gssspi_query_meta_data()
 *     gss_init_sec_context()
 *     gss_inquire_sec_context_by_oid(GSS_C_INQ_NEGOEX_KEY)
 *     gss_inquire_sec_context_by_oid(GSS_C_INQ_NEGOEX_VERIFY_KEY)
 */

OM_uint32 KRB5_CALLCONV
gss_init_sec_context(OM_uint32 *minor_status,
                     gss_cred_id_t claimant_cred_handle,
                     gss_ctx_id_t *context_handle,
                     gss_name_t target_name,
                     gss_OID mech_type,
                     OM_uint32 req_flags,
                     OM_uint32 time_req,
                     gss_channel_bindings_t input_chan_bindings,
                     gss_buffer_t input_token,
                     gss_OID *actual_mech,
                     gss_buffer_t output_token,
                     OM_uint32 *ret_flags,
                     OM_uint32 *time_rec)
{
    return gss_pake_init_sec_context(minor_status,
                                     claimant_cred_handle,
                                     context_handle,
                                     target_name,
                                     mech_type,
                                     req_flags,
                                     time_req,
                                     input_chan_bindings,
                                     input_token,
                                     actual_mech,
                                     output_token,
                                     ret_flags,
                                     time_rec);
}

OM_uint32 KRB5_CALLCONV
gss_accept_sec_context(OM_uint32 *minor_status,
                       gss_ctx_id_t *context_handle,
                       gss_cred_id_t verifier_cred_handle,
                       gss_buffer_t input_token,
                       gss_channel_bindings_t input_chan_bindings,
                       gss_name_t *src_name,
                       gss_OID *mech_type,
                       gss_buffer_t output_token,
                       OM_uint32 *ret_flags,
                       OM_uint32 *time_rec,
                       gss_cred_id_t *delegated_cred_handle)
{
    return gss_pake_accept_sec_context(minor_status,
                                       context_handle,
                                       verifier_cred_handle,
                                       input_token,
                                       input_chan_bindings,
                                       src_name,
                                       mech_type,
                                       output_token,
                                       ret_flags,
                                       time_rec,
                                       delegated_cred_handle);
}

OM_uint32 KRB5_CALLCONV gss_delete_sec_context(OM_uint32 *minor_status,
                                               gss_ctx_id_t *context_handle,
                                               gss_buffer_t output_token)
{
    return gss_pake_delete_sec_context(minor_status,
                                       context_handle,
                                       output_token);
}

OM_uint32 KRB5_CALLCONV gss_acquire_cred(OM_uint32 *minor_status,
                                         gss_name_t desired_name,
                                         OM_uint32 time_req,
                                         gss_OID_set desired_mechs,
                                         gss_cred_usage_t cred_usage,
                                         gss_cred_id_t *output_cred_handle,
                                         gss_OID_set *actual_mechs,
                                         OM_uint32 *time_rec)
{
    return gss_pake_acquire_cred(minor_status,
                                 desired_name,
                                 time_req,
                                 desired_mechs,
                                 cred_usage,
                                 output_cred_handle,
                                 actual_mechs,
                                 time_rec);
}

OM_uint32 KRB5_CALLCONV
gss_acquire_cred_from(OM_uint32 *minor_status,
                      gss_name_t desired_name,
                      OM_uint32 time_req,
                      gss_OID_set desired_mechs,
                      gss_cred_usage_t cred_usage,
                      gss_const_key_value_set_t cred_store,
                      gss_cred_id_t *output_cred_handle,
                      gss_OID_set *actual_mechs,
                      OM_uint32 *time_rec)
{
    /* This is called by spnego to get the list of available mechs */
    if (cred_store == GSS_C_NO_CRED_STORE) {
        return GSS_S_COMPLETE;
    }

    return gss_pake_acquire_cred_from(minor_status,
                                      desired_name,
                                      time_req,
                                      desired_mechs,
                                      cred_usage,
                                      cred_store,
                                      output_cred_handle,
                                      actual_mechs,
                                      time_rec);
}

/* There is no prototype for this function in gssapi/gssapi*.h. However this
 * is what will be dlsym'ed. */
OM_uint32 KRB5_CALLCONV
gssspi_acquire_cred_with_password(OM_uint32 *minor_status,
                               const gss_name_t desired_name,
                               const gss_buffer_t password,
                               OM_uint32 time_req,
                               const gss_OID_set desired_mechs,
                               gss_cred_usage_t cred_usage,
                               gss_cred_id_t *output_cred_handle,
                               gss_OID_set *actual_mechs,
                               OM_uint32 *time_rec);

OM_uint32 KRB5_CALLCONV
gssspi_acquire_cred_with_password(OM_uint32 *minor_status,
                               const gss_name_t desired_name,
                               const gss_buffer_t password,
                               OM_uint32 time_req,
                               const gss_OID_set desired_mechs,
                               gss_cred_usage_t cred_usage,
                               gss_cred_id_t *output_cred_handle,
                               gss_OID_set *actual_mechs,
                               OM_uint32 *time_rec)
{
    return gss_pake_acquire_cred_with_password(minor_status,
                                               desired_name,
                                               password,
                                               time_req,
                                               desired_mechs,
                                               cred_usage,
                                               output_cred_handle,
                                               actual_mechs,
                                               time_rec);
}

OM_uint32 KRB5_CALLCONV gss_release_cred(OM_uint32 *minor_status,
                                         gss_cred_id_t *cred_handle)
{
    return GSS_S_COMPLETE;
}

OM_uint32 KRB5_CALLCONV gss_import_name(OM_uint32 *minor_status,
                                        gss_buffer_t input_name_buffer,
                                        gss_OID input_name_type,
                                        gss_name_t *output_name)
{
    return GSS_S_COMPLETE;
}

OM_uint32 KRB5_CALLCONV gss_release_name(OM_uint32 *minor_status,
                                         gss_name_t *input_name)
{
    return GSS_S_COMPLETE;
}

OM_uint32 KRB5_CALLCONV gss_display_status(OM_uint32 *minor_status,
                                           OM_uint32 status_value,
                                           int status_type,
                                           gss_OID mech_type,
                                           OM_uint32 *message_context,
                                           gss_buffer_t status_string)
{
    return gss_pake_display_status(minor_status,
                                   status_value,
                                   status_type,
                                   mech_type,
                                   message_context,
                                   status_string);
}

OM_uint32 KRB5_CALLCONV gssspi_query_meta_data(OM_uint32 *minor_status,
                                               gss_const_OID mech_oid,
                                               gss_cred_id_t cred_handle,
                                               gss_ctx_id_t *context_handle,
                                               const gss_name_t targ_name,
                                               OM_uint32 req_flags,
                                               gss_buffer_t meta_data)
{
    return gss_pake_query_meta_data(minor_status,
                                    mech_oid,
                                    cred_handle,
                                    context_handle,
                                    targ_name,
                                    req_flags,
                                    meta_data);
}

OM_uint32 KRB5_CALLCONV gssspi_exchange_meta_data(OM_uint32 *minor_status,
                                                  gss_const_OID mech_oid,
                                                  gss_cred_id_t cred_handle,
                                                  gss_ctx_id_t *context_handle,
                                                  const gss_name_t targ_name,
                                                  OM_uint32 req_flags,
                                                  gss_const_buffer_t meta_data)
{
    return GSS_S_COMPLETE;
}

OM_uint32 KRB5_CALLCONV
gssspi_query_mechanism_info(OM_uint32 *minor_status,
                            gss_const_OID mech_oid,
                            unsigned char auth_scheme[16])
{
    return gss_pake_query_mechanism_info(minor_status, mech_oid, auth_scheme);
}

OM_uint32 KRB5_CALLCONV
gss_inquire_sec_context_by_oid(OM_uint32 *minor_status,
                               const gss_ctx_id_t context_handle,
                               const gss_OID desired_object,
                               gss_buffer_set_t *data_set)
{
    return gss_pake_inquire_sec_context_by_oid(minor_status,
                                               context_handle,
                                               desired_object,
                                               data_set);
}
