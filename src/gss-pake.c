/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>
#include "gss-pake.h"

#define MIN(a, b) ((a) <( b) ? (a) : (b))

static gss_OID_desc gss_pake_oid_desc = {
    .elements = discard_const(GSS_PAKE_OID_STRING),
    .length = GSS_PAKE_OID_LENGTH,
};

gss_OID GSS_PAKE_OID = ((gss_OID)&gss_pake_oid_desc);

static bool gssapi_pake_mech_equal(gss_const_OID mech_oid)
{
    int ok;

    ok = gss_oid_equal(mech_oid, GSS_PAKE_OID);
    if (!ok) {
        return false;
    }

    return true;
}

OM_uint32
gss_pake_query_mechanism_info(OM_uint32 *minor_status,
                              gss_const_OID mech_oid,
                              unsigned char auth_scheme[16])
{
    bool ok;

    *minor_status = 0;

    ok = gssapi_pake_mech_equal(mech_oid);
    if (!ok) {
        return GSS_S_BAD_MECH;
    }

    /*
     * auth_scheme is a unique ID to identify the mech, just use our OID for
     * that.
     */
    memset(auth_scheme, 0, 16);
    memcpy(auth_scheme, GSS_PAKE_OID_STRING, MIN(GSS_PAKE_OID_LENGTH, 16));

    return GSS_S_COMPLETE;
}
