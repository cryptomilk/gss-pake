/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <gnutls/crypto.h>
#include <gnutls/gnutls.h>
#include <stdlib.h>
#include <string.h>

#include "gss-pake.h"

static OM_uint32 gss_pake_get_psk(OM_uint32 *minor_status,
                                  gss_cred_id_t cred_handle,
                                  uint8_t **ppsk,
                                  size_t *ppsk_len)
{
    struct gss_pake_cred *cred = NULL;

    if (cred_handle == NULL) {
        *minor_status = GSS_PAKE_STATUS_NO_KEY;
        return GSS_S_FAILURE;
    }
    cred = (struct gss_pake_cred *)cred_handle;

    if (cred->psk.length == 0 || cred->psk.length > 512) {
        *minor_status = GSS_PAKE_STATUS_INVALID_KEY;
        return GSS_S_FAILURE;
    }

    *ppsk = malloc(cred->psk.length);
    if (*ppsk == NULL) {
        *minor_status = GSS_PAKE_STATUS_NO_MEMORY;
        return GSS_S_FAILURE;
    }
    memcpy(*ppsk, cred->psk.data, cred->psk.length);
    *ppsk_len = cred->psk.length;

    return GSS_S_COMPLETE;
}

static OM_uint32 gss_pake_generate_nonce(OM_uint32 *minor_status,
                                         struct gss_pake_ctx *pctx)
{
    int rc;

    rc = gnutls_rnd(GNUTLS_RND_NONCE, pctx->hkdf.salt, sizeof(pctx->hkdf.salt));
    if (rc != 0) {
        *minor_status = GSS_PAKE_STATUS_INTERNAL_ERROR;
        return GSS_S_FAILURE;
    }

    return GSS_S_COMPLETE;
}

static OM_uint32 gss_pake_generate_session_key(OM_uint32 *minor_status,
                                               struct gss_pake_ctx *pctx)
{
    int rc;
    gnutls_datum_t key = {
        .size = pctx->psk.length,
        .data = pctx->psk.data,
    };
    gnutls_datum_t salt = {
        .size = sizeof(pctx->hkdf.salt),
        .data = pctx->hkdf.salt,
    };
    size_t mac_size = gnutls_mac_get_key_size(pctx->hkdf.mac);
    size_t session_key[mac_size];

    if (mac_size < GSS_PAKE_SESSION_KEY_SIZE) {
        *minor_status = GSS_PAKE_STATUS_INTERNAL_ERROR;
        return GSS_S_FAILURE;
    }

    rc = gnutls_hkdf_extract(pctx->hkdf.mac, &key, &salt, session_key);
    if (rc != 0) {
        *minor_status = GSS_PAKE_STATUS_INTERNAL_ERROR;
        return GSS_S_FAILURE;
    }
    memcpy(pctx->negoex_session_key, session_key, GSS_PAKE_SESSION_KEY_SIZE);

    return GSS_S_COMPLETE;
}

static OM_uint32 gss_pake_init_step1(OM_uint32 *minor_status,
                                     gss_cred_id_t claimant_cred_handle,
                                     struct gss_pake_ctx *pctx,
                                     gss_buffer_t output_token)
{
    OM_uint32 major;
    gss_buffer_desc tok;

    pctx->hkdf.mac = GNUTLS_MAC_SHA512;

    major = gss_pake_get_psk(minor_status,
                             claimant_cred_handle,
                             &pctx->psk.data,
                             &pctx->psk.length);
    if (major != GSS_S_COMPLETE) {
        return major;
    }

    major = gss_pake_generate_nonce(minor_status, pctx);
    if (major != GSS_S_COMPLETE) {
        return major;
    }

    major = gss_pake_generate_session_key(minor_status, pctx);
    if (major != GSS_S_COMPLETE) {
        return major;
    }

    tok.length = sizeof(struct gss_pake_hkdf);
    tok.value = &pctx->hkdf;

    major = gss_encapsulate_token(&tok, GSS_PAKE_OID, output_token);
    if (major != GSS_S_COMPLETE) {
        *minor_status = GSS_PAKE_STATUS_INTERNAL_ERROR;
        return major;
    }

    return GSS_S_COMPLETE;
}

OM_uint32 gss_pake_init_sec_context(OM_uint32 *minor_status,
                                    gss_cred_id_t claimant_cred_handle,
                                    gss_ctx_id_t *context_handle,
                                    gss_name_t target_name,
                                    gss_OID mech_type,
                                    OM_uint32 req_flags,
                                    OM_uint32 time_req,
                                    gss_channel_bindings_t input_chan_bindings,
                                    gss_buffer_t input_token,
                                    gss_OID *actual_mech,
                                    gss_buffer_t output_token,
                                    OM_uint32 *ret_flags,
                                    OM_uint32 *time_rec)
{
    struct gss_pake_ctx *pctx = NULL;
    OM_uint32 tmp_minor;
    OM_uint32 major;

    *minor_status = GSS_PAKE_STATUS_OK;

    if (context_handle == NULL) {
        return GSS_S_NO_CONTEXT;
    }

    pctx = (struct gss_pake_ctx *)*context_handle;
    if (pctx == NULL) {
        if (input_token != NULL && input_token->length > 0) {
            *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
            return GSS_S_FAILURE;
        }

        pctx = calloc(1, sizeof(struct gss_pake_ctx));
        if (pctx == NULL) {
            *minor_status = GSS_PAKE_STATUS_NO_MEMORY;
            return GSS_S_FAILURE;
        }
        pctx->initiator = true;

        /* Generate a session key using the PSK */
        major = gss_pake_init_step1(minor_status,
                                    claimant_cred_handle,
                                    pctx,
                                    output_token);
        if (major != GSS_S_COMPLETE) {
            goto done;
        }

    } else {
        if (!pctx->initiator) {
            *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
            major = GSS_S_FAILURE;
            goto done;
        }
    }

    major = GSS_S_COMPLETE;
done:
    if (major != GSS_S_COMPLETE && major != GSS_S_CONTINUE_NEEDED) {
        gss_pake_delete_sec_context(&tmp_minor,
                                    (gss_ctx_id_t *)&pctx,
                                    GSS_C_NO_BUFFER);
    }

    *context_handle = (gss_ctx_id_t)pctx;

    return major;
}

static OM_uint32 gss_pake_accept_step1(OM_uint32 *minor_status,
                                       gss_cred_id_t verifier_cred_handle,
                                       struct gss_pake_ctx *pctx,
                                       gss_buffer_t input_token,
                                       gss_buffer_t output_token)
{
    struct gss_pake_hkdf *hkdf = NULL;
    gss_buffer_desc tok;
    OM_uint32 major;

    major = gss_decapsulate_token(input_token, GSS_PAKE_OID, &tok);
    if (major != GSS_S_COMPLETE) {
        *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
        return major;
    }

    if (tok.length != sizeof(struct gss_pake_hkdf)) {
        *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
        return GSS_S_FAILURE;
    }
    hkdf = tok.value;

    if (hkdf->mac < 5) {
        *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
        return GSS_S_FAILURE;
    }
    pctx->hkdf.mac = hkdf->mac;

    major = gss_pake_get_psk(minor_status,
                             verifier_cred_handle,
                             &pctx->psk.data,
                             &pctx->psk.length);
    if (major != GSS_S_COMPLETE) {
        return major;
    }

    memcpy(pctx->hkdf.salt, hkdf->salt, sizeof(pctx->hkdf.salt));

    major = gss_pake_generate_session_key(minor_status, pctx);
    if (major != GSS_S_COMPLETE) {
        return major;
    }

    return GSS_S_COMPLETE;
}

OM_uint32
gss_pake_accept_sec_context(OM_uint32 *minor_status,
                            gss_ctx_id_t *context_handle,
                            gss_cred_id_t verifier_cred_handle,
                            gss_buffer_t input_token,
                            gss_channel_bindings_t input_chan_bindings,
                            gss_name_t *src_name,
                            gss_OID *mech_type,
                            gss_buffer_t output_token,
                            OM_uint32 *ret_flags,
                            OM_uint32 *time_rec,
                            gss_cred_id_t *delegated_cred_handle)
{
    struct gss_pake_ctx *pctx = NULL;
    OM_uint32 tmp_minor;
    OM_uint32 major;

    *minor_status = GSS_PAKE_STATUS_OK;

    if (context_handle == NULL) {
        *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
        return GSS_S_NO_CONTEXT;
    }

    if (input_token == GSS_C_NO_BUFFER) {
        *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
        return GSS_S_CALL_INACCESSIBLE_WRITE;
    }

    if (src_name != NULL) {
        *src_name = GSS_C_NO_NAME;
    }

    if (mech_type != NULL) {
        *mech_type = GSS_C_NO_OID;
    }

    if (ret_flags != NULL) {
        *ret_flags = 0;
    }

    if (time_rec != NULL) {
        *time_rec = 0;
    }

    pctx = (struct gss_pake_ctx *)*context_handle;
    if (pctx == NULL) {
        if (input_token == NULL || input_token->length == 0) {
            *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
            return GSS_S_FAILURE;
        }

        pctx = calloc(1, sizeof(struct gss_pake_ctx));
        if (pctx == NULL) {
            *minor_status = GSS_PAKE_STATUS_NO_MEMORY;
            return GSS_S_FAILURE;
        }
        pctx->initiator = true;

        major = gss_pake_accept_step1(minor_status,
                                      verifier_cred_handle,
                                      pctx,
                                      input_token,
                                      output_token);
        if (major != GSS_S_COMPLETE) {
            goto done;
        }
    } else {
        if (!pctx->initiator) {
            *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
            major = GSS_S_FAILURE;
            goto done;
        }
    }

    major = GSS_S_COMPLETE;
done:
    if (major != GSS_S_COMPLETE && major != GSS_S_CONTINUE_NEEDED) {
        gss_pake_delete_sec_context(&tmp_minor,
                                    (gss_ctx_id_t *)&pctx,
                                    GSS_C_NO_BUFFER);
    }

    *context_handle = (gss_ctx_id_t)pctx;
    return major;
}

OM_uint32 gss_pake_delete_sec_context(OM_uint32 *minor_status,
                                      gss_ctx_id_t *context_handle,
                                      gss_buffer_t output_token)
{
    struct gss_pake_ctx *pctx = NULL;

    *minor_status = 0;

    if (context_handle == NULL) {
        return GSS_S_COMPLETE;
    }
    pctx = (struct gss_pake_ctx *)*context_handle;

    if (pctx->psk.data != NULL) {
        explicit_bzero(pctx->psk.data, pctx->psk.length);
        free(pctx->psk.data);
    }

    explicit_bzero(pctx->negoex_session_key, GSS_PAKE_SESSION_KEY_SIZE);

    free(pctx);

    *context_handle = GSS_C_NO_CONTEXT;

    return GSS_S_COMPLETE;
}

static OM_uint32 gss_pake_get_session_key(OM_uint32 *minor_status,
                                          const struct gss_pake_ctx *pctx,
                                          gss_buffer_set_t *data_set)
{
    gss_buffer_desc key = {
        .length = sizeof(pctx->negoex_session_key),
        .value = discard_const_p(void, pctx->negoex_session_key),
    };
    uint8_t type_data[4] = {0};
    gss_buffer_desc type = {
        .length = sizeof(type_data),
        .value = type_data,
    };
    OM_uint32 major;
    uint32_t enc_type = 0x0014; /* ENCTYPE_AES256_CTS_HMAC_SHA384_192 */

    type_data[3] = (enc_type >> 24) & 0xff;
    type_data[2] = (enc_type >> 16) & 0xff;
    type_data[1] = (enc_type >> 8) & 0xff;
    type_data[0] = (enc_type)&0xff;

    major = gss_add_buffer_set_member(minor_status, &key, data_set);
    if (major != GSS_S_COMPLETE) {
        return major;
    }
    major = gss_add_buffer_set_member(minor_status, &type, data_set);
    if (major != GSS_S_COMPLETE) {
        return major;
    }

    return GSS_S_COMPLETE;
}

OM_uint32 gss_pake_inquire_sec_context_by_oid(OM_uint32 *minor_status,
                                              const gss_ctx_id_t context_handle,
                                              const gss_OID desired_object,
                                              gss_buffer_set_t *data_set)
{
    const struct gss_pake_ctx *pctx = (struct gss_pake_ctx *)context_handle;
    bool get_key = gss_oid_equal(desired_object, GSS_C_INQ_NEGOEX_KEY);
    bool get_verify_key =
        gss_oid_equal(desired_object, GSS_C_INQ_NEGOEX_VERIFY_KEY);

    if (pctx == NULL) {
        return GSS_S_UNAVAILABLE;
    }

    if (!get_key && !get_verify_key) {
        return GSS_S_UNAVAILABLE;
    }

    return gss_pake_get_session_key(minor_status, pctx, data_set);
}
