/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <gssapi/gssapi_ext.h>
#include <stdbool.h>

/* OID space kindly donated by Samba Project: 1.3.6.1.4.1.7165.655.3 */
#define GSS_PAKE_BASE_OID_STRING "\x2b\x06\x01\x04\x01\xb7\x7d\x85\x0f\x03"
#define GSS_PAKE_BASE_OID_LENGTH 10

/* GSS PAKE OID: 1.3.6.1.4.1.7165.655.2.1 */
#define GSS_PAKE_OID_STRING GSS_PAKE_BASE_OID_STRING "\x01"
#define GSS_PAKE_OID_LENGTH GSS_PAKE_BASE_OID_LENGTH + 1

extern gss_OID GSS_PAKE_OID;

#define discard_const(ptr) ((void *)((uintptr_t)(ptr)))
#define discard_const_p(type, ptr) ((type *)discard_const(ptr))

enum gss_pake_status {
    GSS_PAKE_STATUS_OK = 0,
    GSS_PAKE_STATUS_INVALID_PARAMETER,
    GSS_PAKE_STATUS_NO_MEMORY,
    GSS_PAKE_STATUS_NO_KEY,
    GSS_PAKE_STATUS_INVALID_KEY,
    GSS_PAKE_STATUS_INTERNAL_ERROR,
    GSS_PAKE_STATUS_CRYPTO_SYSTEM_ERROR,
};

struct gss_pake_cred {
    struct {
        size_t length;
        uint8_t *data;
    } psk;
};

#define GSS_PAKE_SESSION_KEY_SIZE 32

struct gss_pake_hkdf {
    uint32_t mac;
    uint8_t salt[16];
};

struct gss_pake_ctx {
    bool initiator;
    struct {
        size_t length;
        uint8_t *data;
    } psk;
    struct gss_pake_hkdf hkdf;
    uint8_t negoex_session_key[GSS_PAKE_SESSION_KEY_SIZE];
};

OM_uint32
gss_pake_acquire_cred(OM_uint32 *minor_status,
                      gss_name_t desired_name,
                      OM_uint32 time_req,
                      gss_OID_set desired_mechs,
                      gss_cred_usage_t cred_usage,
                      gss_cred_id_t *output_cred_handle,
                      gss_OID_set *actual_mechs,
                      OM_uint32 *time_rec);

OM_uint32
gss_pake_acquire_cred_from(OM_uint32 *minor_status,
                           gss_name_t desired_name,
                           OM_uint32 time_req,
                           gss_OID_set desired_mechs,
                           gss_cred_usage_t cred_usage,
                           gss_const_key_value_set_t cred_store,
                           gss_cred_id_t *output_cred_handle,
                           gss_OID_set *actual_mechs,
                           OM_uint32 *time_rec);

OM_uint32
gss_pake_acquire_cred_with_password(OM_uint32 *minor_status,
                                    const gss_name_t desired_name,
                                    const gss_buffer_t password,
                                    OM_uint32 time_req,
                                    const gss_OID_set desired_mechs,
                                    gss_cred_usage_t cred_usage,
                                    gss_cred_id_t *output_cred_handle,
                                    gss_OID_set *actual_mechs,
                                    OM_uint32 *time_rec);

OM_uint32 gss_pake_release_cred(OM_uint32 *minor_status,
                                gss_cred_id_t *cred_handle);

OM_uint32 gss_pake_init_sec_context(OM_uint32 *minor_status,
                                    gss_cred_id_t claimant_cred_handle,
                                    gss_ctx_id_t *context_handle,
                                    gss_name_t target_name,
                                    gss_OID mech_type,
                                    OM_uint32 req_flags,
                                    OM_uint32 time_req,
                                    gss_channel_bindings_t input_chan_bindings,
                                    gss_buffer_t input_token,
                                    gss_OID *actual_mech,
                                    gss_buffer_t output_token,
                                    OM_uint32 *ret_flags,
                                    OM_uint32 *time_rec);

OM_uint32
gss_pake_accept_sec_context(OM_uint32 *minor_status,
                            gss_ctx_id_t *context_handle,
                            gss_cred_id_t verifier_cred_handle,
                            gss_buffer_t input_token,
                            gss_channel_bindings_t input_chan_bindings,
                            gss_name_t *src_name,
                            gss_OID *mech_type,
                            gss_buffer_t output_token,
                            OM_uint32 *ret_flags,
                            OM_uint32 *time_rec,
                            gss_cred_id_t *delegated_cred_handle);

OM_uint32 gss_pake_delete_sec_context(OM_uint32 *minor_status,
                                      gss_ctx_id_t *context_handle,
                                      gss_buffer_t output_token);

OM_uint32 gss_pake_display_status(OM_uint32 *minor_status,
                                  OM_uint32 status_value,
                                  int status_type,
                                  gss_OID mech_type,
                                  OM_uint32 *message_context,
                                  gss_buffer_t status_string);

OM_uint32 gss_pake_query_mechanism_info(OM_uint32 *minor_status,
                                        gss_const_OID mech_oid,
                                        unsigned char auth_scheme[16]);

OM_uint32 gss_pake_query_meta_data(OM_uint32 *minor_status,
                                   gss_const_OID mech_oid,
                                   gss_cred_id_t cred_handle,
                                   gss_ctx_id_t *context_handle,
                                   const gss_name_t targ_name,
                                   OM_uint32 req_flags,
                                   gss_buffer_t meta_data);

OM_uint32 gss_pake_exchange_meta_data(OM_uint32 *minor_status,
                                      gss_const_OID mech_oid,
                                      gss_cred_id_t cred_handle,
                                      gss_ctx_id_t *context_handle,
                                      const gss_name_t targ_name,
                                      OM_uint32 req_flags,
                                      gss_const_buffer_t meta_data);

OM_uint32 gss_pake_inquire_sec_context_by_oid(OM_uint32 *minor_status,
                                              const gss_ctx_id_t context_handle,
                                              const gss_OID desired_object,
                                              gss_buffer_set_t *data_set);
