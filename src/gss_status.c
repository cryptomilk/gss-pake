/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "gss-pake.h"

struct _gss_pake_status {
    enum gss_pake_status code;
    const char *str;
};

static const struct _gss_pake_status _pake_status_table[] = {
    {
        .code = GSS_PAKE_STATUS_OK,
        .str = "GSS_PAKE_STATUS_OK",
    },
    {
        .code = GSS_PAKE_STATUS_NO_MEMORY,
        .str = "Out of memory",
    },
    {
        .code = GSS_PAKE_STATUS_INVALID_PARAMETER,
        .str = "Invalid parameter",
    },
    {
        .code = GSS_PAKE_STATUS_NO_KEY,
        .str = "No pre-shared key set",
    },
    {
        .code = GSS_PAKE_STATUS_INVALID_KEY,
        .str = "The pre-shared key is invalid",
    },
    {
        .code = GSS_PAKE_STATUS_INTERNAL_ERROR,
        .str = "An internal error occured",
    },
    {
        .code = GSS_PAKE_STATUS_CRYPTO_SYSTEM_ERROR,
        .str = "An error in the crypto system occured",
    },
    {
        .code = 0,
        .str = NULL,
    },
};

OM_uint32 gss_pake_display_status(OM_uint32 *minor_status,
                                  OM_uint32 status_value,
                                  int status_type,
                                  gss_OID mech_type,
                                  OM_uint32 *message_context,
                                  gss_buffer_t status_string)
{
    size_t idx;

    if (status_string == NULL) {
        *minor_status = EINVAL;
        return GSS_S_CALL_INACCESSIBLE_READ;
    }

    if (status_type != GSS_C_MECH_CODE) {
        *minor_status = EINVAL;
        return GSS_S_BAD_STATUS;
    }

    if (status_value == GSS_PAKE_STATUS_OK) {
        return GSS_S_COMPLETE;
    }

    for (idx = 0; _pake_status_table[idx].str != NULL; idx++) {
        if (status_value == _pake_status_table[idx].code) {
            status_string->length = strlen(_pake_status_table[idx].str);
            status_string->value = strdup(_pake_status_table[idx].str);;

            if (status_string->value == NULL) {
                *minor_status = ENOMEM;
                return GSS_S_FAILURE;
            }
        }
    }

    return GSS_S_COMPLETE;
}
