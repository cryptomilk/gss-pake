/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>

#include "gss-pake.h"

OM_uint32
gss_pake_acquire_cred_from(OM_uint32 *minor_status,
                           gss_name_t desired_name,
                           OM_uint32 time_req,
                           gss_OID_set desired_mechs,
                           gss_cred_usage_t cred_usage,
                           gss_const_key_value_set_t cred_store,
                           gss_cred_id_t *output_cred_handle,
                           gss_OID_set *actual_mechs,
                           OM_uint32 *time_rec)
{
    OM_uint32 major;
    struct gss_pake_cred *cred = NULL;
    size_t i;

    if (cred_store == NULL || cred_store->count == 0) {
        *minor_status = GSS_PAKE_STATUS_INVALID_PARAMETER;
        return GSS_S_CRED_UNAVAIL;
    }

    cred = calloc(1, sizeof(struct gss_pake_cred));
    if (cred == NULL) {
        *minor_status = GSS_PAKE_STATUS_NO_MEMORY;
        return GSS_S_FAILURE;
    }

    for (i = 0; i < cred_store->count; i++) {
        gss_key_value_element_desc element = cred_store->elements[i];
        int cmp;

        cmp = strcmp(element.key, "psk");
        if (cmp == 0) {
            cred->psk.length = strlen(element.value);
            cred->psk.data = (uint8_t *)strndup(element.value, cred->psk.length);
            if (cred->psk.data == NULL) {
                *minor_status = GSS_PAKE_STATUS_NO_MEMORY;
                major = GSS_S_FAILURE;
                goto done;
            }
        }
    }

    if (cred->psk.length == 0) {
        *minor_status = GSS_PAKE_STATUS_NO_KEY;
        major = GSS_S_CRED_UNAVAIL;
        goto done;
    }

    major = GSS_S_COMPLETE;
done:
    if (major == GSS_S_COMPLETE) {
        *output_cred_handle = (gss_cred_id_t)cred;
    } else {
        uint32_t tmp_min = 0;
        gss_release_cred(&tmp_min, (gss_cred_id_t *)&cred);
    }

    return GSS_S_COMPLETE;
}

OM_uint32
gss_pake_acquire_cred(OM_uint32 *minor_status,
                      gss_name_t desired_name,
                      OM_uint32 time_req,
                      gss_OID_set desired_mechs,
                      gss_cred_usage_t cred_usage,
                      gss_cred_id_t *output_cred_handle,
                      gss_OID_set *actual_mechs,
                      OM_uint32 *time_rec)
{
    return gss_pake_acquire_cred_from(minor_status,
                                      desired_name,
                                      time_req,
                                      desired_mechs,
                                      cred_usage,
                                      GSS_C_NO_CRED_STORE,
                                      output_cred_handle,
                                      actual_mechs,
                                      time_rec);
}

OM_uint32
gss_pake_acquire_cred_with_password(OM_uint32 *minor_status,
                                    const gss_name_t desired_name,
                                    const gss_buffer_t password,
                                    OM_uint32 time_req,
                                    const gss_OID_set desired_mechs,
                                    gss_cred_usage_t cred_usage,
                                    gss_cred_id_t *output_cred_handle,
                                    gss_OID_set *actual_mechs,
                                    OM_uint32 *time_rec)
{
    gss_key_value_element_desc element = {
        .key = "psk",
    };
    gss_key_value_set_desc cred_store = {
        .count = 1,
        .elements = &element,
    };

    if (password == NULL || password->value == NULL) {
        return GSS_S_NO_CRED;
    }

    element.value = (const char *)password->value;

    return gss_pake_acquire_cred_from(minor_status,
                                      desired_name,
                                      time_req,
                                      desired_mechs,
                                      cred_usage,
                                      &cred_store,
                                      output_cred_handle,
                                      actual_mechs,
                                      time_rec);
}

OM_uint32 gss_pake_release_cred(OM_uint32 *minor_status,
                                gss_cred_id_t *cred_handle)
{
    struct gss_pake_cred *cred = NULL;

    if (cred_handle == NULL) {
        return GSS_S_COMPLETE;
    }

    cred = (struct gss_pake_cred *)*cred_handle;

    if (cred->psk.length > 0) {
        explicit_bzero(cred->psk.data, cred->psk.length);
        free(cred->psk.data);
    }
    free(cred);

    return GSS_S_COMPLETE;
}
