# - Try to find GnuTLS
# Once done this will define
#
#  GNUTLS_FOUND - system has GnuTLS
#  GNUTLS_INCLUDE_DIR - the GnuTLS include directory
#  GNUTLS_LIBRARIES - Link these to use GnuTLS
#  GNUTLS_DEFINITIONS - Compiler switches required for using GnuTLS
#  GNUTLS_VERSION - Version number of GnuTLS
#
#=============================================================================
#  Copyright (c) 2022 Andreas Schneider <asn@cryptomilk.org>
#
#  Distributed under the OSI-approved BSD License (the "License");
#  see accompanying file Copyright.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even the
#  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the License for more information.
#=============================================================================
#

if (UNIX)
    find_package(PkgConfig)
    if (PKG_CONFIG_FOUND)
        pkg_check_modules(PKG_GNUTLS gnutls)
    endif()

    set(GNUTLS_VERSION ${PKG_GNUTLS_VERSION})
    set(GNUTLS_DEFINITIONS ${PKG_GNUTLS_DEFINITIONS})
endif()

find_path(GNUTLS_INCLUDE_DIR
    NAMES
        gnutls/gnutls.h
    PATHS
        ${PKG_GNUTLS_INCLUDEDIR}
)

find_library(GNUTLS_LIBRARY
    NAMES
        gnutls
    PATHS
        ${PKG_GNUTLS_LIBDIR}
)

if (GNUTLS_LIBRARY)
    list(APPEND GNUTLS_LIBRARIES ${GNUTLS_LIBRARY})
endif (GNUTLS_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GnuTLS
                                  FOUND_VAR GNUTLS_FOUND
                                  REQUIRED_VARS GNUTLS_INCLUDE_DIR GNUTLS_LIBRARIES
                                  VERSION_VAR GNUTLS_VERSION)

# show the GNUTLS_INCLUDE_DIR and GNUTLS_LIBRARIES variables only in the advanced view
mark_as_advanced(GNUTLS_INCLUDE_DIR GNUTLS_LIBRARIES)
