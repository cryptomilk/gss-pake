GSS-PAKE SSP
============

The GSSAPI Password-Authenticated Key ExchangeJosé Security Support Provider.

## Requirements

* [MIT Kerberos](http://web.mit.edu/kerberos/)
* [GnuTLS](https://gnutls.org/)
